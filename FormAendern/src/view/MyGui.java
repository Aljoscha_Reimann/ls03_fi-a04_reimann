package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import java.awt.Font;

public class MyGui extends JFrame {

	private JPanel pnlMain;
	private JTextField txtHierBitteText;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MyGui frame = new MyGui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MyGui() {
		setTitle("Form_aendern");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 384, 492);
		pnlMain = new JPanel();
		pnlMain.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(pnlMain);
		pnlMain.setLayout(null);
		
		JLabel lblDieserTextSoll = new JLabel("Dieser Text soll ver\u00E4ndert werden.");
		lblDieserTextSoll.setBounds(21, 6, 325, 50);
		pnlMain.add(lblDieserTextSoll);
		
		JButton btnRot = new JButton("Rot");
		btnRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pnlMain.setBackground(Color.RED);
			}
		});
		btnRot.setBounds(20, 72, 102, 26);
		pnlMain.add(btnRot);
		
		JButton btnGrn = new JButton("Gr\u00FCn");
		btnGrn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pnlMain.setBackground(Color.GREEN);
			}
		});
		btnGrn.setBounds(131, 72, 102, 26);
		pnlMain.add(btnGrn);
		
		JButton btnBlau = new JButton("Blau");
		btnBlau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pnlMain.setBackground(Color.BLUE);
			}
		});
		btnBlau.setBounds(243, 72, 102, 26);
		pnlMain.add(btnBlau);
		
		JButton btnGelb = new JButton("Gelb");
		btnGelb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pnlMain.setBackground(Color.YELLOW);
			}
		});
		btnGelb.setBounds(20, 109, 102, 26);
		pnlMain.add(btnGelb);
		
		JButton btnFarbeWhlen = new JButton("Farbe w\u00E4hlen");
		btnFarbeWhlen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pnlMain.setBackground(JColorChooser.showDialog(lblDieserTextSoll, "Neue Farbe w�hlen", Color.white));
			}
		});
		btnFarbeWhlen.setBounds(243, 109, 102, 26);
		pnlMain.add(btnFarbeWhlen);
		
		JButton btnStandartfarbe = new JButton("Standardfarbe");
		btnStandartfarbe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pnlMain.setBackground(UIManager.getColor("menu"));
			}
		});
		btnStandartfarbe.setBounds(131, 109, 102, 26);
		pnlMain.add(btnStandartfarbe);
		
		JButton btnArial = new JButton("Arial");
		btnArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Font old = lblDieserTextSoll.getFont();
				Font arial = new Font("Arial", old.getStyle(), old.getSize());
				lblDieserTextSoll.setFont(arial);
			}
		});
		btnArial.setBounds(20, 165, 102, 26);
		pnlMain.add(btnArial);
		
		JButton btnCourierNew = new JButton("Courier New");
		btnCourierNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Font old = lblDieserTextSoll.getFont();
				Font CourierNew = new Font("Courier New", old.getStyle(), old.getSize());
				lblDieserTextSoll.setFont(CourierNew);
			}
		});
		btnCourierNew.setBounds(243, 165, 102, 26);
		pnlMain.add(btnCourierNew);
		
		JButton btnComicSansMs = new JButton("Comic Sans MS");
		btnComicSansMs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Font old = lblDieserTextSoll.getFont();
				Font ComicSansMS = new Font("Comic Sans MS", old.getStyle(), old.getSize());
				lblDieserTextSoll.setFont(ComicSansMS);
			}
		});
		btnComicSansMs.setFont(new Font("Tahoma", Font.PLAIN, 10));
		btnComicSansMs.setBounds(131, 165, 102, 26);
		pnlMain.add(btnComicSansMs);
		
		JButton button_8 = new JButton("Rot");
		button_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setForeground(Color.RED);
			}
		});
		button_8.setBounds(20, 280, 102, 26);
		pnlMain.add(button_8);
		
		JButton btnGrn_1 = new JButton("Schwarz");
		btnGrn_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setForeground(Color.BLACK);
			}
		});
		btnGrn_1.setBounds(243, 280, 102, 26);
		pnlMain.add(btnGrn_1);
		
		JButton btnBlau_1 = new JButton("Blau");
		btnBlau_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setForeground(Color.BLUE);
			}
		});
		btnBlau_1.setBounds(131, 280, 102, 26);
		pnlMain.add(btnBlau_1);
		
		JButton btnLinksbndig = new JButton("linksb\u00FCndig");
		btnLinksbndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setHorizontalAlignment(SwingConstants.LEFT);	
			}
		});
		btnLinksbndig.setBounds(20, 377, 102, 26);
		pnlMain.add(btnLinksbndig);
		
		JButton btnRechtsbndig = new JButton("rechtsb\u00FCndig");
		btnRechtsbndig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setHorizontalAlignment(SwingConstants.RIGHT);
			}
		});
		btnRechtsbndig.setBounds(243, 377, 102, 26);
		pnlMain.add(btnRechtsbndig);
		
		JButton btnZentriert = new JButton("zentriert");
		btnZentriert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setHorizontalAlignment(SwingConstants.CENTER);
			}
		});
		btnZentriert.setBounds(131, 377, 102, 26);
		pnlMain.add(btnZentriert);
		
		JLabel lblHintergrundfarbendern = new JLabel("Hintergrundfarbe \u00E4ndern:");
		lblHintergrundfarbendern.setBounds(21, 56, 156, 14);
		pnlMain.add(lblHintergrundfarbendern);
		
		JLabel lblNewLabel = new JLabel("Text formatieren:");
		lblNewLabel.setBounds(21, 146, 133, 14);
		pnlMain.add(lblNewLabel);
		
		JButton btnExit = new JButton("EXIT");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(1);
			}
		});
		btnExit.setBounds(73, 419, 222, 23);
		pnlMain.add(btnExit);
		
		txtHierBitteText = new JTextField();
		txtHierBitteText.setBounds(20, 202, 325, 20);
		pnlMain.add(txtHierBitteText);
		txtHierBitteText.setColumns(10);
		
		JButton btnInsLabelSchreiben = new JButton("Ins Label schreiben");
		btnInsLabelSchreiben.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String txteingabe = txtHierBitteText.getText();
				lblDieserTextSoll.setText(txteingabe);
			}
		});
		btnInsLabelSchreiben.setBounds(20, 233, 157, 26);
		pnlMain.add(btnInsLabelSchreiben);
		
		JButton btnTextImLabel = new JButton("Text im Label l\u00F6schen");
		btnTextImLabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblDieserTextSoll.setText("");
			}
		});
		btnTextImLabel.setBounds(187, 233, 157, 26);
		pnlMain.add(btnTextImLabel);
		
		JButton button = new JButton("+");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Font old = lblDieserTextSoll.getFont();
				Font size = new Font(old.getFamily(), old.getStyle(), old.getSize()+1);
				lblDieserTextSoll.setFont(size);
			}
		});
		button.setBounds(20, 329, 157, 26);
		pnlMain.add(button);
		
		JButton button_1 = new JButton("-");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Font old = lblDieserTextSoll.getFont();
				Font size = new Font(old.getFamily(), old.getStyle(), old.getSize()-1);
				lblDieserTextSoll.setFont(size);
			}
		});
		button_1.setBounds(187, 329, 157, 26);
		pnlMain.add(button_1);
		
		JLabel lblSchriftgreVerndern = new JLabel("Schriftgr\u00F6\u00DFe ver\u00E4ndern:");
		lblSchriftgreVerndern.setBounds(21, 310, 166, 14);
		pnlMain.add(lblSchriftgreVerndern);
		
		JLabel lblSchriftfarbendern = new JLabel("Schriftfarbe \u00E4ndern:");
		lblSchriftfarbendern.setBounds(21, 264, 145, 14);
		pnlMain.add(lblSchriftfarbendern);
		
		JLabel lblNewLabel_1 = new JLabel("Textausrichtung:");
		lblNewLabel_1.setBounds(21, 360, 122, 14);
		pnlMain.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Programm beenden");
		lblNewLabel_2.setBounds(141, 404, 154, 14);
		pnlMain.add(lblNewLabel_2);
	}
}
