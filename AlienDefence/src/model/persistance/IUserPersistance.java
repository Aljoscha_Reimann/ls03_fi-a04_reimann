package model.persistance;

import model.User;

public interface IUserPersistance {
	int createUser(User user);
	void updateUser(User user);
	void deleteUser(User user);
	User readUser(String username);

}